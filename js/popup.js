class Popup {
    constructor() {
        self = this;

        $(document).on('DOMContentLoaded', () => {
            self.RestoreOptions();
            // self.SendDiscordMsg();
            $('#switch__auto_farm').on('change', (event) => {
                if ($(event.currentTarget).is(':checked')) {
                    self.SendMsg(true);
                } else {
                    self.SendMsg(false);
                }
            });
        });
    }
    /**
     * Send message to chrome storage
     * @param {int} switchVal 
     */
    SendMsg(switchState) {

        chrome.storage.sync.set({ switch: switchState });

        const msg = {
            from: 'popup',
            switch: switchState
        }

        chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
            chrome.tabs.sendMessage(tabs[0].id, msg, (response) => { });
        });
    }
    /**
     * Keep switch state when popup is close and reopen
     */
    RestoreOptions() {
        chrome.storage.sync.get(['switch'], (items) => {
            $("#switch__auto_farm").prop('checked', items.switch);
        });
    }
    /**
     * When tab is realod reset storage info
     */
    // DetectReloadTab() {
    //     chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    //         alert(tabId, changeInfo, tab);
    //         if (changeInfo.url === undefined) {
    //             chrome.storage.sync.set({ 'switch': false });
    //         }
    //     });
    // }
    /**
     * Send discord message
     */
    SendDiscordMsg() {
        const request = new XMLHttpRequest();
        request.open("POST", "https://discord.com/api/webhooks/800376197224595458/IFTxVJ8ehvbkYAGGGtURpkiXRFGstMgslORtE6upkVrVQiRp4abSX3YcdyYJJvutqzbL");

        request.setRequestHeader('Content-type', 'application/json');

        const myEmbed = {
            author: {
                name: "Luteur21"
            },
            title: "Test",
            description: "This is a test!",
            color: self.HexToDecimal("#ff0000")
        }

        const params = {
            embeds: [myEmbed]
        }

        request.send(JSON.stringify(params));
    }
    /**
     * Convert hexadecimal to decimal
     * @param {string} hex 
     */
    HexToDecimal(hex) {
        return parseInt(hex.replace("#", ""), 16)
    }
}

new Popup({});