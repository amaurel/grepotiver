class AutoFarm {
    constructor() {
        self = this;

        self.CooldownExecute();
    }
    /**
     * Take farm every 10 min
     */
    CooldownExecute() {
        setTimeout(() => { self.GetFarm(); }, 500);
        setInterval(() => { self.GetFarm(); }, 300000);
    }
    /**
     * Open farm box and get ressources
     */
    GetFarm() {
        self.Sleep(self.RandomVal(2000, 3000)).then(() => {
            FarmTownOverviewWindowFactory.openFarmTownOverview()
            self.Sleep(self.RandomVal(800, 1200)).then(() => {
                $('#fto_town_wrapper .select_all').trigger('click');
                self.Sleep(self.RandomVal(800, 1200)).then(() => {
                    $('#farm_town_options #fto_claim_button').trigger('click');
                    self.Sleep(self.RandomVal(800, 1200)).then(() => {
                        $('.btn_confirm').trigger('click');
                        self.Sleep(self.RandomVal(800, 1200)).then(() => {
                            $('.ui-dialog').hide();
                        });
                    });
                });
            });
        });
    }
    /**
     * Generate random number between 2 values
     * @param {int} min 
     * @param {int} max 
     */
    RandomVal(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    /**
     * Generate delay
     * @param {int} ms 
     */
    Sleep(ms) {
        return (
            new Promise((resolve, reject) => {
                setTimeout(() => { resolve(); }, ms);
            })
        );
    }
}

new AutoFarm({});
