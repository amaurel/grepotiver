class Content {
    constructor() {
        self = this;

        self.InjectScript('js/towns.js', 'towns-info');
        self.PopupListener();
        self.InsertHtmlBox();
    }
    /**
     * Get popup changes
     */
    PopupListener() {
        chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
            if (request.switch === true) {
                self.InjectScript('index-es5.js', 'grepotiver');
            } else {
                self.RemoveScript('grepotiver');
            }
            self.InsertValIntoBox(request.from, request.switch);
            console.log(request, sender, sendResponse);
            return Promise.resolve("Keep alive");
        });
    }
    /**
     * Inject index script into target page
     */
    // InjectBulma() {
    //     const link = document.createElement('link');
    //     link.rel = "stylesheet";
    //     link.type = "text/css";
    //     link.href = chrome.extension.getURL('node_modules/bulma/css/bulma.min.css');
    //     (document.head || document.documentElement).appendChild(link);
    // }
    /**
     * Inject index script into target page
     */
    InjectScript(file_name, id) {
        const script = document.createElement('script');
        script.type = "text/javascript";
        script.src = chrome.extension.getURL(file_name);
        script.id = id;
        (document.head || document.documentElement).appendChild(script);
    }
    /**
     * Remove index script from target page
     */ 
    RemoveScript(id) {
        const script = document.getElementById(id);
        script.remove();
    }
    /**
     * Insert into grepolis html a new box
     */
    InsertHtmlBox() {
        const baseLocation = $('#ui_box');
        const box = `
            <div id="ext_box_display" style="height: 40px; width: 130px; position: absolute; top: 222px; z-index: 5; background-color: #000; padding: 5px;"></div>
        `
        baseLocation.find('.nui_left_box').after(box);
        baseLocation.find('.nui_main_menu').css("top", "272px");
    }
    /**
     * Insert values into new box
     */
    InsertValIntoBox(elementValue, switchValue) {
        const values = `
            <span style="text-align: center; color: #fff; font-weight: 600; font-size: 8px;">${elementValue}</span>
            </br>
            <span style="text-align: center; color: #fff; font-weight: 600">${switchValue}</span>
        `
        $('#ext_box_display').empty();
        $('#ext_box_display').append(values);
    }
}

new Content({});