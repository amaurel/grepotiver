class Towns {
    constructor() {
        self = this;

        $('.town_name_area .btn_toggle_town_groups_menu .button.js-button-caption').on('click', () => {
            self.AppendFreePop();
        });
    }
    /**
     * Get towns
     */
    GetTowns() {
        const towns = ITowns.towns_collection.models;
        const towns_arr = [];

        towns.forEach(element => {
            townsArr.push(element);
        });
        return towns_arr;
    }
    /**
     * Get town
     * @param {int} town_id 
     */
    GetTown(town_id) {
        const towns = ITowns.towns_collection.models;

        towns.forEach(element => {
            element.attributes.id === town_id;
            return element;
        });
    }
    /**
     * Append new div with free pop indicator
     */
    AppendFreePop() {
        console.log('1');
        $('#town_groups_list .town_group_town').each(element => {
            try {
                const town_item = $(element);
                const town_id = town_item.attr('name');
                const town_info = self.GetTown(town_id);

                console.log(town_item);

                element.prepend(`
                    <div class="free-pop">${town_info.attributes.available_population}</div>
                `);
            } catch (error) {
                errorHandling(error);
            }

        });
    }
}

new Towns({});